# Schomp Digital  Marketing Specialist#

##Instructions##

This simple html coding challenge, represents a basic level of skill needed to succeed as a Digital Marketing Specialist at Schomp Automotive. Please utilize the documents in this directory and complete the project "KPI HTML EMAIL TEMPLATE". 

1. Code the email based on the Design Brief and using the Asset Info documents. 
2. Please start with the html document provided. This is just a base template to get you started. Please conform any code to produce a valid document and the desired completion of the project.
3. Confirm no html errors.


###Bonus ###
1. Source UTM links with info from Asset Info Doc
2. Add Additional design than what is provided. 



####Notes####
1. Images provided must be absolute. Please use links provided.
2. HTML for emails best practices are desirable. Please caution your code. 
3. Creativity - If you want to embellish to show your skills off, please create addition assets - okay use relative links for these.


##Asset Information##

###Website URL:###
============

https://www.schompbmw.com/

###Offers URL: ###
============

https://www.schompbmw.com/dealer-specials/



###Absolute Image URL's:###
=====================

1. https://promo.schomp.com/email/bmw/2018/dist/assets/img/headerBanner.jpg
2. https://promo.schomp.com/email/bmw/2018/dist/assets/img/ultimate-care.jpg
3. https://promo.schomp.com/email/bmw/2018/dist/assets/img/schompbmw-logo.jpg



###Store Contact Info:###
=====================

1190 Plum Valley Lane
Highlands Ranch, CO 80129
303.730.1300



###Bonus - Set up Google Link Tags###
=================================

source=socket
medium=email
campaign=m08-w33-kpi



#Design Brief#
============


###Project###
=========

We need a html email template to be created for KPI leads that come from BMW USA. It is very important to have a strong template to engage these leads/customers. 



###Here is the copy:###
=================

Hello ~FIRST NAME~,

This is ~SALESPERSON~ with Schomp BMW. Thank you for expressing interest in purchasing or leasing a new BMW MODEL.  We appreciate you giving us the opportunity to earn your business.

A perk of driving a new BMW is BMW Ultimate Care: a complimentary manufacturer scheduled maintenance program for the first 3 years or 36K miles, whichever comes first. 
 
As a token of our gratitude, we�re pleased to offer you a complimentary maintenance upgrade to BMW Ultimate Care+ with the purchase or lease of any New BMW. 

BMW Ultimate Care+ is a $600 value that reduces your out-of-pocket maintenance expenses during your ownership. 

It extends coverage to specific items that need replacement due to normal wear and tear and that aren�t covered by the original New Vehicle Limited Warranty. BMW Ultimate Care+ covers items such as brake pads, brake rotors, clutch, wiper blade inserts, and any applicable adjustments required due to normal operating conditions.

There are a number of manufacturer programs available: Finance/Purchase credits up to $3,000, Lease Credits up to $2,000 and other manufacturer incentives that WILL APPLY.

*These offers expire on August 31, 2018.

Feel free to reach out to me with any questions about qualifications. I can be reached at ~SALESPERSON PHONE NUMBER~ or by simply replying to this email. Thanks again. I look forward to visiting with you and helping you find your next vehicle.




At Schomp BMW, we provide our clients a unique and revolutionary One Price. One Person. One  Hour.� sales experience. We work diligently to find the most cost effective way for you to lease or purchase your BMW by exploring all current manufacturer programs and incentives that are available. 


Best Regards,
 
~Sales Person~
Schomp BMW Client Advisor




###Images must be place in absolute URLs:###
======================================

1. https://promo.schomp.com/email/bmw/2018/dist/assets/img/headerBanner.jpg
2. https://promo.schomp.com/email/bmw/2018/dist/assets/img/ultimate-care.jpg
3. https://promo.schomp.com/email/bmw/2018/dist/assets/img/schompbmw-logo.jpg
